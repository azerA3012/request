import React,{useState,useEffect} from "react";
import './fullPost.css'
import axios from 'axios'
const FullPost = (props) => {
    const [loadedPost,setLoadedPost]=useState([])
    
   
    useEffect(()=>{
      if(props.postId!=null){
        axios.get(`https://jsonplaceholder.typicode.com/posts/${props.postId}`)
      .then(res=>{
        
       console.log(res);

      })
      }
      
    },[])
    
    

 
  return (
    <div className="full-post">
      <h1>title</h1>
      <h1>{props.postId}</h1>
      <p>body</p>
      <div className="Edit">
        <button className="Delete">Delete</button>
      </div>
    </div>
  );
};

export default FullPost;
