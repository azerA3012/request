import React from 'react';
import './newPost.css'
const NewPost = () => {
    return ( 
        <div className="new-post">
            <h1>add a post</h1>
            <label>title</label>
            <input type="text"/>
            <label>content</label>
            <textarea/>
            <label>author</label>
            <select>
                <option value=""></option>
                <option value=""></option>
            </select>
            <button>add post</button>
        </div>
     );
}
 
export default NewPost;