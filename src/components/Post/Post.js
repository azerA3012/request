import React from 'react';
import './post.css'
const Post = (props) => {
    return ( 
        <div className="post" onClick={props.clicked}>
            <h1>{props.title}</h1>
            <div className="info">
                <div className="author">Author</div>
            </div>
        </div>
     );
}
 
export default Post;