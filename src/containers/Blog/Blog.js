import React,{useState,useEffect} from 'react';
import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import axios from 'axios'

const Blog = () => {
    
    //Post State
    const [post,setPost]=useState([])
    useEffect(()=>{
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(res=>{
            const updatePosts=res.data.slice(0,4)
            
            setPost([...updatePosts])
        })
        
    },[])

    //id State
    const [postId,setPostId]=useState(null)

    const postSelected=(id)=>{
       

        setPostId(id)
    }

    const posts=post.map(post=>{

        return <Post key={post.id} title={post.title} clicked={()=>postSelected(post.id)} />
    })
    
    return ( 
        
        <div>
            <section className="d-flex justify-content-center flex-wrap">
                {posts}
               
            </section>
            <section className="d-flex justify-content-center">
                <FullPost postId={postId}/>
            </section>
            <section className="d-flex justify-content-center">
                <NewPost/>
            </section>
        </div>
     );
}
 
export default Blog;
